const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'development';
const isProduction = nodeEnv === 'isProduction';
const isLocalDevelopment = nodeEnv === 'development';

let plugins = [
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'src/index.html.ejs'),
    inject: 'body',
  }),
  new webpack.DefinePlugin({
    IS_PRODUCTION: isProduction
  }),
];

if (isLocalDevelopment) {
  plugins = plugins.concat([
    new webpack.HotModuleReplacementPlugin()
  ])
}

module.exports = {
  devtool: !isProduction ? '#source-map' : '',
  context: path.resolve(__dirname, 'src'),
  entry: {
    'app': path.resolve(__dirname, 'src/index.ts')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].bundle.js',
  },
  resolve: {
    extensions: ['.js', '.scss', '.ts', '.tsx', '.styl'],
    alias: {
      components: path.resolve(__dirname, 'src/components'),
      shared: path.resolve(__dirname, 'src/shared'),
      store: path.resolve(__dirname, 'src/store'),
    }
  },
  plugins,
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        enforce: 'pre',
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [
          {
            loader: 'tslint-loader',
            options: {
              tsConfigFile: './tsconfig.json',
              failOnHint: false,
            }
          }
        ]
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [
          'cache-loader',
          'ts-loader',
        ],
      },
      {
        test: /\.styl$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              url: true,
              importLoaders: 2,
              modules: {
                mode: 'local',
                localIdentName: '[name]-[local]-[hash:base64:6]',
              },
              localsConvention: 'camelCase'
            },
          },
          {
            loader: 'stylus-loader',
          },
        ],
      }
    ]
  },
  devServer: {
    port: 3330,
    host: '0.0.0.0',
    historyApiFallback: true,
    stats: {
      children: false,
    }
  },
  performance: {
      hints: isProduction ? "warning" : false
  },
  optimization: {
    // minimize: isProduction
  },
  mode: nodeEnv
};
