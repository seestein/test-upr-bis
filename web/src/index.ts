import * as React from 'react';
import {render} from 'react-dom';
import {Router} from 'components/router/Router';

const element = document && document.getElementById('app');

if (!element) {
  throw Error('Should never happen');
}

render(React.createElement(Router), element);

// check if HMR is enabled
if (module.hot) {
  // accept itself
  module.hot.accept();
}
