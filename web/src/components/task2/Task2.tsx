import * as React from 'react';
import {VictoryPie} from 'victory';
import * as styles from './task2.module.styl';

function getCharsNumber(text: string): ICharsNumber {
  const result = {};

  for (const char of text) {
    result[char] = (result[char] || 0) + 1;
  }

  return result;
}

interface ICharsNumber {
  [key: string]: number;
}

export class Task2 extends React.PureComponent<ITask2Props, IState> {
  constructor(props: ITask2Props) {
    super(props);

    this.state = {
      text: '',
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({text: e.target.value});
  }

  render() {
    const charsNumber = getCharsNumber(this.state.text);

    return (
      <React.Fragment>
        <h1>2 задание</h1>
        <p>
          Дано поле в которое можно вводить произвольные символы. Необходимо подсчитать количество введенных букв
          (a, b, c) и отобразить введенное количество букв на экране. Дополнительно нужно подключить какую-нибудь
          библиотеку с графиками и отобразить на Pie диаграмме процентное соотношение между введенными
          символами (a, b, c). Реализовать динамический пересчет значений/графика.
        </p>

        <h4>Демо</h4>

        <div>
          <input
            name='text'
            value={this.state.text}
            onChange={this.handleChange}
            style={{width: 300, height: 20}}
          />
        </div>

        <h5>Результат</h5>

        {!!this.state.text.length && (
          <React.Fragment>
            <div className={styles.wrapper}>
              <div className={styles.firstChild}>
                Всего символов: {this.state.text.length}<br/>
                Кол-во отдельных символов:<br/>

                {Object.entries(charsNumber).map(([char, charNumber]) => (
                  <div key={char}>
                    {char}: {charNumber}
                  </div>
                ))}
              </div>
              <div className={styles.secondChild}>
                <VictoryPie
                  colorScale={['tomato', 'orange', 'gold', 'cyan', 'navy', 'green', 'blue', 'yellow']}
                  data={Object.entries(charsNumber).map(([char, charNumber]) => (
                    {label: char, y: charNumber}
                  ))}
                  style={{
                    data: {
                      fillOpacity: 0.9, stroke: '#c43a31', strokeWidth: 1,
                    },
                    parent: {
                      width: 400,
                    },
                  }}
                />
              </div>
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

interface ITask2Props {

}

interface IState {
  text: string;
}
