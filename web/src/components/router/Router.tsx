import * as React from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import {Root} from 'components/root/Root';
import {Task1} from '../task1/Task1';
import {Task2} from '../task2/Task2';

export const Router = () => (
  <BrowserRouter>
    <Root>
      <Switch>
        <Route exact path='/' component={Task1} />
        <Route path='/task2' component={Task2} />
        <Route render={() => (<div>Not found</div>)} />
      </Switch>
    </Root>
  </BrowserRouter>
);
