import * as React from 'react';
import {Link} from 'react-router-dom';

export interface IProps {}

export class Root extends React.Component<IProps> {
  render() {
    return (
      <div>
        <div style={{padding: 20}}>
          <Link to='/'>1 задание (общее)</Link>&nbsp;|&nbsp;
          <Link to='/task2'>2 задание (React)</Link>
        </div>

        <hr />

        <div style={{padding: '0px 20px'}}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
