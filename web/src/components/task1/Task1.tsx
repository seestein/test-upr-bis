import * as React from 'react';

function compare(str1: string, str2: string): string {
  const commonString = `${str1}${str2}`;

  let arr = commonString.split('');
  arr = [...new Set(arr)];

  arr.sort((a, b) => {
    if (a < b) {
      return -1;
    }

    if (a > b) {
      return 1;
    }

    return 0;
  });

  return arr.join('');
}

export class Task1 extends React.PureComponent<ITask1Props, IState> {
  constructor(props: ITask1Props) {
    super(props);

    const a = 'xyaabbbccccdefww';
    const b = 'xxxxyyyyabklmopq';
    console.log(compare(a, b), compare(a, b) === 'abcdefklmopqwxy');

    const c = 'abcdefghijklmnopqrstuvwxyz';
    console.log(compare(c, c), compare(c, c) === 'abcdefghijklmnopqrstuvwxyz');

    this.state = {
      string1: '',
      string2: '',
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({[e.target.name]: e.target.value} as {[K in keyof IState]: IState[K]});
  }

  render() {
    return (
      <React.Fragment>
        <h1>1 задание</h1>
        <p>
          Возьмите две строки s1 и s2, в которые включены только буквы от a до z. Необходимо вернуть новую
          отсортированную строку, максимально длинную, содержащую только уникальные буквы.
          Решение должно быть написано на языке TypeScript и в коде придерживаться основным
          принципам функционального программирования.
        </p>

        <h4>Пример</h4>

        <code>
          // 1<br/>
          const a = 'xyaabbbccccdefww';<br/>
          const b = 'xxxxyyyyabklmopq';<br/><br/>
          console.log(compare(a, b), compare(a, b) === 'abcdefklmopqwxy'); // abcdefklmopqwxy true<br/><br/>

          // 2<br/>
          const c = "abcdefghijklmnopqrstuvwxyz"<br/><br/>
          console.log(compare(c, c), compare(c, c) === 'abcdefghijklmnopqrstuvwxyz'); // abcdefghijklmnopqrstuvwxyz true
        </code>

        <h4>Демо</h4>

        <div>
          Строка A:<br/>
          <input
            name='string1'
            value={this.state.string1}
            onChange={this.handleChange}
            style={{width: 300, height: 20}}
          />
        </div>

        <div>
          Строка B:<br/>
          <input
            name='string2'
            value={this.state.string2}
            onChange={this.handleChange}
            style={{width: 300, height: 20}}
          />
        </div>

        <h5>Результат</h5>

        {compare(this.state.string1, this.state.string2)}
      </React.Fragment>
    );
  }
}

interface ITask1Props {

}

interface IState {
  string1: string;
  string2: string;
}
