declare var IS_PRODUCTION: boolean;

declare module '*.styl';

declare module '*module.styl' {
  const classes: { [key: string]: string };
  export default classes;
}
